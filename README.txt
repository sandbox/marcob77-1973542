--------------------------------------------------------------------------------------
                                   ABOUT THE MODULE

restrict_admin_ip allow you to define a range of ip addresses for administrator role: only user's who come from this ip address can login with administration
role  

                                  INSTALLATION INSTRUCTIONS
  ------------------------------------------------------------------------------------
To use this module, first of all you have to add wich  user ip can reach the site with administration role, in settings.php

You can add 2 lines of code 
example with exact ip:
$conf['allow_administrator_ip_addresses']= '10.73.16.77|10.73.16.75';

example in cidr format:
$conf['allow_administrator_class_addresses_cidr']='10.73.20.76/30|10.73.20.120/30';

After this, just turn the module on in the usual Drupal way.